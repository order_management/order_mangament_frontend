import Header from 'components/layout/Header';
import Sidebar from 'components/layout/Sidebar';
import React from 'react';
import { Outlet } from 'react-router-dom';
import styled from 'styled-components';

interface IMainLayoutProps {
    children?: React.ReactNode
}

const MainStyles = styled.main`
    .main-wrapper {
        background: #ececec;
    }
`;

const MainLayout = ({children} : IMainLayoutProps) => {
    return (
        <div className='row min-vh-100 g-0'>
            <div className="col-2 shadow-sm">
                <Sidebar />
            </div>
            <div className="col-10 d-flex flex-column">
                <Header />
                <MainStyles className='flex-grow-1'>
                    <div className='main-wrapper h-100 p-3'>
                        <Outlet></Outlet>
                    </div>
                </MainStyles>
            </div>
        </div>
    );
};

export default MainLayout;