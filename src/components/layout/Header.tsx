import React from 'react';
import styled from 'styled-components';

const HeaderStyles = styled.header`
    height: 80px;
    padding: 20px;
`;

const Header = () => {
    return (
        <HeaderStyles className='bg-white d-flex justify-content-end align-items-center'>
            Admin
        </HeaderStyles>
    );
};

export default Header;