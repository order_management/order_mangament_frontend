
import MainLayout from "layouts/MainLayout";
import { lazy, Suspense } from "react";
import { Route, Routes } from "react-router-dom";

const Dashboard = lazy(() => import('pages/Dashboard'));
const OrdersPage = lazy(() => import('pages/Orders'));
const CustomersPage = lazy(() => import('pages/Customers'));
const ReturnOrdersPage = lazy(() => import('pages/ReturnOrders'));

const App = () => {
	return (
		<Suspense fallback={<></>}>
			<Routes>
				<Route element={<MainLayout></MainLayout>}>
					<Route path="/" element={<Dashboard />}></Route>
					<Route path="/orders" element={<OrdersPage />}></Route>
					<Route path="/customers" element={<CustomersPage />}></Route>
					<Route path="/return-orders" element={<ReturnOrdersPage />}></Route>
				</Route>
			</Routes>
		</Suspense>
	);
};

export default App;
