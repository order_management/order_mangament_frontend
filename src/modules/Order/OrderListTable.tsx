import { Link } from 'react-router-dom';
import { renderStatus } from 'utils/renderOrderStatus';

interface IOrderListTableProps {
    orders: any[]
}

const OrderListTable = ({orders} : IOrderListTableProps) => {
    const totalPaymentAmount = (orders.reduce((total, order) => total + order.final_price, 0)).toFixed(4);

    return (
        <>
            <table className="table table-hover">
                <thead className='table-primary fw-semibold'>
                    <tr className='text-white'>
                        <th>Mã đơn hàng</th>
                        <th>Ngày lên đơn</th>
                        <th>Tên khách hàng</th>
                        <th>Tổng thanh toán</th>
                        <th>Nhân viên sale</th>
                        <th>Địa chỉ</th>
                        <th>Trạng thái</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody className='table-body'>
                    {orders.length > 0 && orders.map((order) => (
                        <tr key={order.id}>
                            <td>{order?.order_code}</td>
                            <td>{order?.created_at}</td>
                            <td>{order?.customer_name}</td>
                            <td>{order?.final_price}</td>
                            <td>{order?.sale_name}</td>
                            <td>{order?.address}</td>
                            <td>{renderStatus(order?.status)}</td>
                            <td>
                                <ul className="navbar-nav">
                                    <li className="nav-item dropdown">
                                        <Link to={'/'} className="text-black text-decoration-none dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                            Thao tác
                                        </Link>
                                        <ul className="dropdown-menu dropdown-menu-primary">
                                            <li><Link to={`/orders/${order.id}`} className="dropdown-item">Chi tiết</Link></li>
                                            <li><Link className="dropdown-item" to="#">Thu tiền</Link></li>
                                        </ul>
                                    </li>
                                </ul> 
                            </td>
                        </tr>
                    ))}
                    <tr></tr>
                </tbody>
                <tfoot className='table-secondary'>
                    <tr>
                        <td className='fw-semibold'>Tổng</td>
                        <td></td>
                        <td></td>
                        <td className='fw-semibold'>{totalPaymentAmount}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </>
    );
};

export default OrderListTable;