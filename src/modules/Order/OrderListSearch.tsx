import IconSearch from 'components/icons/IconSearch';
import React from 'react';

const OrderListSearch = () => {
    return (
        <div className="input-group mb-3">
            <input type="text" className="form-control" placeholder="Search..." />
            <span className="input-group-text">
                <IconSearch></IconSearch>
            </span>
        </div>
    );
};

export default OrderListSearch;