import axios from 'axios';
import IconPlus from 'components/icons/IconPlus';
import IconReload from 'components/icons/IconReload';
import Pagination from 'components/layout/Pagination';
import { API_URL } from 'config';
import { defaultPagination } from 'constants/general.const';
import { useQuery } from 'hooks/useQuery';
import OrderListSearch from 'modules/Order/OrderListSearch';
import OrderListTable from 'modules/Order/OrderListTable';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { IPagination } from 'types/pagination.type';

const ActionsStyles = styled.div`
    a {
        width: 40px;
        height: 40px;
    }
`;

const Orders = () => {
    const [orders, setOrders] = useState([]);
    const [pagination, setPagination] = useState<IPagination>(defaultPagination);
    const query = useQuery();
    const page = query.get('page') || '1';

    useEffect(() => {
        async function getOrdersList() {
            try {
                const response = await axios.get(`${API_URL}/order/list?limit=5${page ? `&page=${page}` : ''}`);
                
                if (response.status === 200 && response.data.data) {
                    setOrders(response.data.data);
                    setPagination(response.data.meta);
                }
            } catch (error) {
                console.log(error);
            }
        };

        getOrdersList();
    }, [page]);

    return (
        <div className='bg-white h-100 p-3'>
            <div className="row">
                <div className="col-9">
                    <div className="row">
                        <div className="col-4">
                            <OrderListSearch></OrderListSearch>
                        </div>
                        <div className="col-8"></div>
                    </div>
                </div>
                <ActionsStyles className="col-3 d-flex justify-content-end gap-2">
                    <Link to='/orders/add' className='btn btn-primary d-flex align-items-center justify-content-center'>
                        <IconPlus />
                    </Link>
                    <Link to={''} className='btn btn-outline-primary d-flex align-items-center justify-content-center'>
                        <IconReload />
                    </Link>
                </ActionsStyles>
            </div>
            <OrderListTable orders={orders}></OrderListTable>
            <Pagination pagination={pagination}></Pagination>
        </div>
    );
};

export default Orders;